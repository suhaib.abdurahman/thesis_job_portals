
# -*- coding: utf-8 -*-
import scrapy
import hashlib
import pandas as pd

def getUrls():
    #load company names in kununu data set -> look for those companies job postings
    keywords = pd.read_csv("../raw_data/kununu_df.csv", index_col=0).Org.drop_duplicates().tolist()
    search_base = 'http://www.stepstone.de/5/ergebnisliste.html?stf=freeText&ns=1&qs=%5B%5D&companyID=0&cityID=0&sourceOfTheSearchField=homepagemex%3Ageneral&searchOrigin=Homepage_top-search&ke='
    urls = []
    for i in keywords:
        searchwords = i.replace(' ', '+')
        urls.append(search_base + '"'+ searchwords + '"')
        
    return urls

class scrapeStepStoneJobs(scrapy.Spider): 
    name = 'stepstone'                      
    allowed_domains = ["stepstone.de"]
    start_urls = getUrls()
    BASE_URL = "https://www.stepstone.de"
    page_nr = 0 #initialise
    
    def parse(self, response): #find all links to job ads and download the whole website
        for url in response.xpath('//div[@class="sc-fzXfOw cvFCUL"]/a/@href').extract(): #extract from first page
            absolute_url = self.BASE_URL + url
            yield scrapy.Request(absolute_url, self.download_website)
        #look for next page button
        next_page_link = response.xpath('//a[contains(@data-at, "pagination-next")]/@href').extract() #list (of 1)
        if next_page_link: #if not on last page
            yield scrapy.Request(next_page_link[0], self.next_page)    
    
    def next_page(self, response):
        for url in response.xpath('//div[@class="sc-fzXfOw cvFCUL"]/a/@href').extract(): #extract from first page
            absolute_url = self.BASE_URL + url
            yield scrapy.Request(absolute_url, self.download_website)
        next_page_link = response.xpath('//a[contains(@data-at, "pagination-next")]/@href').extract() #list (of 1)
        if next_page_link and self.page_nr <= 5: #look for more websites on the next 6 pages (some companies/keywords have too many results!)
            self.page_nr = self.page_nr + 1        
            yield scrapy.Request(next_page_link[0], self.next_page)  
        else:
            self.page_nr = 0 

    def download_website(self, response):
        filename = response.url.split("/")[-1] #datei name
        hashed_name = hashlib.sha1(filename.encode("utf-8")).hexdigest() + ".html" #save as .html
        with open("../raw_data/websites/" + hashed_name, 'wb') as f:
            f.write(response.body)

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    