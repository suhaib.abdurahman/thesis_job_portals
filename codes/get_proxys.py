#keep separate, mention in documentation

import requests                        #access websites
from fake_useragent import UserAgent
#OS and file system access
import os
from pathlib import Path               #access to file system
main_path = str(Path(os.getcwd()).parents[0]) #path to mainfolder

import csv
import re

# settings for scraping

def getLinks(): #gibt alle links zu stellenanzeigen wieder -> https://www.stepstone.de/ davor setzen
    header = {"user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36" ,'referer':'https://www.google.com/'}
    ua = UserAgent()
    for i in range(10):
        userAgent = ua.random
        header = {"user-agent": userAgent, 'referer':'https://www.google.com/'}
        source = str(requests.get("https://free-proxy-list.net", headers=header).text)
        data = [list(filter(None, i))[0] for i in re.findall('<td class="hm">(.*?)</td>|<td>(.*?)</td>', source)]
        groupings = [dict(zip(['ip', 'port', 'code', 'using_anonymous'], data[i:i+4])) for i in range(0, len(data), 4)]
        final_groupings = ["http://{ip}:{port}".format(**i) for i in groupings]
    return final_groupings

links = getLinks()

with open("../stepstone/stepstone/proxy_list.txt", 'w') as output:
    wr = csv.writer(output, delimiter = "\n")
    wr.writerow(links)