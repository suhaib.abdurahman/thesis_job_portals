# Introduction

This repository contains the necessary code and data to reproduce the results presented in the bachelor's thesis of Suhaib Abdurahman (2021, Predicting job-ratings from job-postings using natural language processing).

# Contents

This repo contains four jupyter notebooks:

- `pipeline_processing_texts.ipynb`
- `pipeline_predictions.ipynb`
- `pipeline_processing_wordclouds.ipynb`
- `pipeline_visualisations.ipynb`

The purpose of the first notebook is to pre-process the text data (job-postings) and create the text embeddings from these texts.
The second notebook runs the prediction model, saves the results and visualises the performance.
The third notebook pre-processes the job-postings texts for the visualisations.
The fourth notebook visualises the data through a multitude of plots (heatmaps, word clouds, etc.)
All notebooks contain extensive commentary on the nature of the code and further instructions for the execution if neccessary.

The repo further contains various auxiliary files for the proper functioning of some packages. It also contains the pre-trained embeddings model (USE, multi-language).

Lastly, the repo contains all the neccessary data to produce the predictions. 
It contains the originally scraped websites containing the job-postings as well as the data frame with the kununu ratings.

# Set-up

Firstly, this thesis was programmed using python. I recommend setting up a new environment using a manager such as anaconda. This will make it easier to install and manage the necesseray packages.
Second, the code is provided as four jupyter notebooks, which I recommend to install using a manager such as anaconda as well.

In order to run the predictions and to create the plots, proceed in the following order:

1. Install python and jupyter notebook on your local machine (preferably through anaconda).
2. Clone this repo to you local machine.
3. Execute `pipeline_processing_texts.ipynb` to preprocess the text data and create the text embeddings. This also downloads the industry labels.
  3.1 Manually fix the industry labels were necessary (kununu might have mixed up the classification) and simplify the categories according to the german federal office for statistics
  3.2 Save as `industries_manuel_fix.csv` in the misc folder. However, a prepaired version has already been added to the data folder as `industries_manuel_fix.csv` in the misc folder! So this step has not to be made from scratch.
4. Execute `pipeline_predictions.ipynb` to run the prediction tasks and save the results.
5. Execute `pipeline_processing_wordclouds.ipynb` to preprocess the text data for word cloud visualisations.
6. Execute `pipeline_visualisations.ipynb` to create the visualisations of the job-postings and job-ratings data.
	
For subsequent runs, and plots only the prediction visualisation codes need to be executed. All necessary pre-processed data will be saved after the first run.
